package com.module5.com;

public class Main {

    public static void main(String[] args) {
	Player ploo = new Player(114, "Plooful", 77);
	Player wardell = new Player(4, "TSM Wardell", 120);
	Player shroud = new Player(1, "Shroud", 500);
	Player skadoodle= new Player(6, "Ska", 450);
	Player hazed = new Player(29, "hazed", 369);
	Player jimmy = new Player(44, "deadlyJimmy", 77);

	SimpleHashtable hashtable = new SimpleHashtable();

	hashtable.put(ploo.getUserName(), ploo);
	hashtable.put(wardell.getUserName(), wardell);
	hashtable.put(shroud.getUserName(), shroud);
	hashtable.put(skadoodle.getUserName(), skadoodle);
	hashtable.put(hazed.getUserName(), hazed);
	hashtable.put(jimmy.getUserName(), jimmy);

	hashtable.printHashtable();

	System.out.println(hashtable.get("deadlyJimmy"));

	hashtable.remove(jimmy.getUserName(), jimmy);
	hashtable.printHashtable();
    }
}

package com.midterm.exam;

import java.util.EmptyStackException;

public class CardArrayStack {
    private  Card[] stack;
    private int top;

    public CardArrayStack(int capacity)
    {
        stack = new Card[capacity];
    }

    public void push(Card card)
    {
        if (top == stack.length) // stack = full
        {
            Card[] newStack = new Card[2 * stack.length];
            System.arraycopy(stack, 0, newStack, 0, stack.length);
            stack = newStack;
        }

        stack[top++] = card;
    }

    public Card pop()
    {
        if(isEmpty()) { throw new EmptyStackException(); }
        Card poppedCard = stack[--top];
        stack[top] = null;

        return poppedCard;
    }

    public Card peek()
    {
        if(isEmpty()) { throw new EmptyStackException(); }

        return stack[top - 1];
    }

    public void printStack()
    {
        if(isEmpty()) { throw new EmptyStackException(); }
        else {
            for (int i = top - 1; i >= 0; i--) {
                System.out.println(stack[i]);
            }
        }
    }

    public int stackCount()
    {
        int counter = 0;
        if(isEmpty()) { throw new EmptyStackException(); }
        else {
            for (int i = top - 1; i >= 0; i--) {
                counter++;
            }
            return counter;
        }
    }

    public boolean isEmpty() { return top == 0; }
}

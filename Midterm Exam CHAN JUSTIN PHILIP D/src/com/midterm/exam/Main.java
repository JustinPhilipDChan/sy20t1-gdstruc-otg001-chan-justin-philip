package com.midterm.exam;

public class Main {

    public static void main(String[] args) {
        CardArrayStack playerDeck = new CardArrayStack(30);
        CardArrayStack discardPile = new CardArrayStack(30);
        CardArrayStack hand = new CardArrayStack(30);

        //add 30 cards into playerDeck (hard code)
        playerDeck.push(new Card("Ace", 1));
        playerDeck.push(new Card("Two", 2));
        playerDeck.push(new Card("Three", 3));
        playerDeck.push(new Card("Four", 4));
        playerDeck.push(new Card("Five", 5));
        playerDeck.push(new Card("Six", 6));
        playerDeck.push(new Card("Seven", 7));
        playerDeck.push(new Card("Eight", 8));
        playerDeck.push(new Card("Nine", 9));
        playerDeck.push(new Card("Ten", 10));
        playerDeck.push(new Card("Ace", 11));
        playerDeck.push(new Card("Two", 12));
        playerDeck.push(new Card("Three", 13));
        playerDeck.push(new Card("Four", 14));
        playerDeck.push(new Card("Five", 15));
        playerDeck.push(new Card("Six", 16));
        playerDeck.push(new Card("Seven", 17));
        playerDeck.push(new Card("Eight", 18));
        playerDeck.push(new Card("Nine", 19));
        playerDeck.push(new Card("Ten", 20));
        playerDeck.push(new Card("Ace", 21));
        playerDeck.push(new Card("Two", 22));
        playerDeck.push(new Card("Three", 23));
        playerDeck.push(new Card("Four", 24));
        playerDeck.push(new Card("Five", 25));
        playerDeck.push(new Card("Six", 26));
        playerDeck.push(new Card("Seven", 27));
        playerDeck.push(new Card("Eight", 28));
        playerDeck.push(new Card("Nine", 29));
        playerDeck.push(new Card("Ten", 30));



        do
        {
            System.out.print("The program will give out three possible commands:\n");
            System.out.print("1.) Draw a random number of cards\n2.) Discard x number of cards\n3.) Get any number of cards from the discarded pile\n\n");

            int command = (int) (Math.random() * 3) + 1;
            int randomizer = (int) (Math.random() * 5) + 1;

            System.out.println("A.I. picked " + command +".)\n");

            if (command == 1)
            {
                System.out.println(randomizer + " card(s) will be drawn from the deck.");
                if (!playerDeck.isEmpty()) {
                   for (int i = 1; i <= randomizer; i++) {
                       if (i <= playerDeck.stackCount()) {
                           System.out.println(playerDeck.peek() + " was drawn!\n");
                           hand.push(playerDeck.peek());
                           playerDeck.pop();
                       }
                   }
                }
               else {
                    System.out.println("Player Deck is Empty!\n");
                   System.out.println("\nPress Any Key To Continue...\n");
                    new java.util.Scanner(System.in).nextLine();
               }
            }
            else if (command == 2)
            {
                if (!hand.isEmpty()) {
                    System.out.println(randomizer + " card(s) will be discarded.");
                    for (int i = 1; i <= randomizer; i++) {
                        if (i <= hand.stackCount()) {
                            System.out.println(hand.peek() + " from the players hand was discarded!\n");
                            discardPile.push(hand.peek());
                            hand.pop();
                        }
                    }
                }
                else {
                    System.out.println("Randomizer drew a " + randomizer + "\nBut the Player doesn't have any cards");
                }
            }
            else if (command == 3)
            {
                if (!discardPile.isEmpty()) {
                    System.out.println(randomizer + " card(s) will be drawn from the discard pile.");
                    for (int i = 1; i <= randomizer; i++) {
                        if (i <= discardPile.stackCount()) {
                            System.out.println(discardPile.peek() + " was drawn from the Discarded Pile!\n");
                            hand.push(discardPile.peek());
                            discardPile.pop();
                        }
                    }
                }
                else {
                    System.out.println("Randomizer drew a " + randomizer + "\nBut the Discarded Pile is empty!");
                }

            }

            //round over
            System.out.println("\nRound over.\nPress Any Key To Continue...\n");
            new java.util.Scanner(System.in).nextLine();
            clearScreen();

            //List of hand stack
            System.out.println("Player Hand: ");
            if (hand.isEmpty()) { System.out.println("Empty\n"); } // if empty
            else { hand.printStack(); }

            //Remaining cards in the deck
            System.out.println("\nNumber of cards left in the deck: " );
            if (playerDeck.isEmpty()) { System.out.println("\nEmpty\n"); } // if empty
            else {
                System.out.println(playerDeck.stackCount());
            }

            //Number of cards in the discarded pile
            System.out.println("\nNumber of cards in the discarded pile: " );
            if (discardPile.isEmpty()) { System.out.println("Empty\n"); } // if empty
            else { System.out.println(discardPile.stackCount()); }

            System.out.println("\nPress Any Key To Continue...\n");
            new java.util.Scanner(System.in).nextLine();
            clearScreen();
        } while (!playerDeck.isEmpty());

        System.out.println("Thank you for playing!\n\nPlayer Deck is now empty.\nGame Over.");

        System.out.println("\nPress Any Key To Continue...\n");
        new java.util.Scanner(System.in).nextLine();
    }

    public static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
}


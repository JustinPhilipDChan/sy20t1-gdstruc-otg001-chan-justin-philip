package com.company;

public class Main {

    public static void main(String[] args) {
	    int[] numbers = new int [10];
	    
        numbers[0] = 66;
        numbers[1] = -44;
        numbers[2] = 890;
        numbers[3] = 5;
        numbers[4] = -1;
        numbers[5] = 0;
        numbers[6] = 122;
        numbers[7] = 28;
        numbers[8] = 73;
        numbers[9] = 10;

        printArray(numbers);
        modifiedSelectionSort(numbers);
        printArray(numbers);

    }

    private static void bubbleSort(int[] arr) // descending order
    {
        for (int last = arr.length - 1; last > 0; last--)
        {
            for (int i = 0; i < last; i++)
            {
                if (arr[i] < arr[i + 1])
                {
                    int temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                }
            }
        }
    }

    private static void selectionSort(int[] arr) // descending order
    {
        for (int last = arr.length - 1; last > 0; last--)
        {
            int largetIndex = 0;

            for (int i = 1; i <= last; i++)
            {
                if (arr[i] < arr[largetIndex])
                {
                    largetIndex = i;
                }
            }

            int temp = arr[last];
            arr[last] = arr[largetIndex];
            arr[largetIndex] = temp;
        }
    }

    private static void modifiedSelectionSort(int[] arr) // smallest = end of the array
    {
         for (int last = arr.length - 1; last > 0; last--)
         {
             int smallestIndex = 0;

             for (int i = 1; i <= last; i++)
             {
                 if (arr[i] < arr[smallestIndex])
                 {
                     smallestIndex = i;
                 }
             }

        int temp = arr[last];
        arr[last] = arr[smallestIndex];
        arr[smallestIndex] = temp;
    }
}

    private static void printArray(int[] arr) // print
    {
        for (int j : arr) {
            System.out.println(j);
        }
        System.out.print("\n");
    }
}

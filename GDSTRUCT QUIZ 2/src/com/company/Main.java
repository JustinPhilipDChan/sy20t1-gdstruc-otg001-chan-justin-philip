package com.company;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

//        List<Player> playerList = new ArrayList<>();
//
//        playerList.add(new Player(1, "Asuna", 100));
//        playerList.add(new Player(2, "LethalBacon", 205));
//        playerList.add(new Player(3, "HPDeskJet", 34));
//
//        for(Player p : playerList)
//        {
//            System.out.println(p);
//        }
        Player hpDeskJet = new Player(3, "HpDeskJet", 34);
        Player lethalBacon = new Player(2, "LethalBacon", 205);
        Player asuna = new Player(1, "Asuna", 10);
        Player kirito = new Player(4, "Kirito", 560);

        PlayerLinkedList playerLinkedList = new PlayerLinkedList();

        playerLinkedList.addToFront(asuna);
        playerLinkedList.addToFront(lethalBacon);
        playerLinkedList.addToFront(kirito);
        playerLinkedList.addToFront(hpDeskJet);
        System.out.print("Initial linked list information: \n");
        playerLinkedList.printList();

        System.out.print("\nLinked List size: ");

        playerLinkedList.listSize();

        playerLinkedList.removeFirst();

        System.out.print("\nLinked list after removing the first node:\n");

        playerLinkedList.printList();

        System.out.print("\nLinked List size after removing the first node: ");

        playerLinkedList.listSize();

        //add another Node to confirm if listSize works after adding a Node

        playerLinkedList.addToFront(hpDeskJet);

        System.out.print("\nLinked List size after adding a Node: ");
        playerLinkedList.listSize();

        System.out.print("\nLink list contains 'HpDeskJet': ");

        playerLinkedList.contains(hpDeskJet);

        System.out.print("\nLinked List index of 'Asuna': ");

        playerLinkedList.indexOf(asuna);

    }
}

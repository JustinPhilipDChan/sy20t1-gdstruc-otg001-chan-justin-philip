package com.company;

import java.util.Objects;

public class PlayerLinkedList {
    private PlayerNode head;

    public void addToFront(Player player) {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;

    }

    public void printList() {
        PlayerNode current = head;
        System.out.print("Head -> ");
        while(current != null) {
            System.out.print(current);
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }
        System.out.println("null");
    }

    public PlayerNode removeFirst() {
        if (head == null) return null;

        PlayerNode temp = head;
        head = head.getNextPlayer();
        return head;
    }

    public int listSize() {
        int size = 0;
        PlayerNode current = head;

        while (current != null) {
            size++;
            current = current.getNextPlayer();
        }
        System.out.println(size);
        return size;
    }

    public void contains(Player player) {

    boolean contain = false;
    PlayerNode current = head;

    if (current.getPlayer() == player) contain = true;
    else if (current.getPlayer() == null) contain = false;
    else current = current.getNextPlayer();

    System.out.println(contain);
   }

   public int indexOf(Player player){
        PlayerNode current = head;
        int count = 0;

        while (current.getPlayer() != null) {

            if(current.getPlayer() == player) {
                System.out.println(count);
                return count;
            }

            current = current.getNextPlayer();
            count++;
        }

        System.out.println(count);
        return count;
   }
}

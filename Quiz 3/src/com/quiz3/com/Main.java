package com.quiz3.com;

import java.sql.Array;

public class Main {

    public static void main(String[] args) {
	    ArrayQueue queue = new ArrayQueue(10);
        int games = 0;

        String[] names = new String[10];
        names[0] = "aceu";
        names[1] = "Wardell";
        names[2] = "n0thing";
        names[3] = "jasonR";
        names[4] = "tenZ";
        names[5] = "AZK";
        names[6] = "shanks";
        names[7] = "Freakzoid";
        names[8] = "Stewie2k";
        names[9] = "taric";

        System.out.println("Queue simulator");
        System.out.println("\nPress Any Key To Continue...\n");
        new java.util.Scanner(System.in).nextLine();

        do {
            //turn
            do {
                int randomizer = (int) (Math.random() * 7) + 1;

                for (int i = 0; i <= randomizer; i++) {
                    queue.add(new Player(i, names[(int) (Math.random() * (9))], 100));
                }
                System.out.println("\nCurrently in Queue:\n");
                queue.printQueue();
                System.out.println("\nPress Any Key To Continue...\n");
                new java.util.Scanner(System.in).nextLine();

            } while (queue.size() < 5);

            System.out.println("The game has started!\n\nThe first 5 players are in game!\n");

            //pop first 5 players
            for(int l = 0; l < 5; l++) {
                queue.remove();
            }

            games++;

            if (games == 10) { break;}

        }while (games < 10);

        System.out.println("10 games have been completed.\n");
;
        System.out.println("\nPress Any Key To Continue...\n");
        new java.util.Scanner(System.in).nextLine();
    }
}
